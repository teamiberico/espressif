//***CODIGO PARA EL NODEMCU***//
//***DESARROLLADO POR DANIEL COUSIÑO Y NOEL SEARA***//

//***LIBRERIAS***//
#include <ESP8266WiFi.h>
extern "C" {
  #include <espnow.h>
}

//***VARIABLES***//
#define CHANNEL 5
bool engine = false;

//***METODO INICIALIZACIÓN DEL PROTOCOLO ESP-NOW***//  
void InitESPNow() {
  if (esp_now_init() == 0) {
     Serial.println("*** ESPNow Init Success");
  }
  else {
    Serial.println("*** ESPNow Init Failed");
    ESP.restart();  //Reinicia el nodeMCU
  }
}

void setup() {

  //***INICIALIZACIÓN DEL PUERTO SERIE***//
    Serial.begin(115200); delay(10);
    Serial.println("");
    
  //***INICIALIZACIÓN DEL LED***//
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

  //***INICIALIZACIÓN DEL ESP-NOW***//
    InitESPNow();

  //***DATOS DE LA MAC del NODEMCU***// 
    Serial.print("Access Point MAC: "); Serial.println(WiFi.softAPmacAddress());
    Serial.println("---------------------------------------------------------------------------------");

  //***DECLARACIÓN DEL ROL DEL NODEMCU EN LA COMUNICACIÓN ESP-NOW***//
    //0=OCIOSO, 1=MAESTRO, 2=ESCLAVO y 3=MAESTRO+ESCLAVO 
    esp_now_set_self_role(3);   

  //***EMPAREJAMIENTO CON EL ESCLAVO***//
    uint8_t mac_addr[6] = {0x30, 0xAE, 0xA4, 0x37, 0x97, 0x91}; //Dirección MAC STA del ESP32 esclavo
    uint8_t role = 2;  
    uint8_t channel = CHANNEL;
    uint8_t key[0] = {};   //no hay clave 
    //uint8_t key[16] = {0,255,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
    uint8_t key_len = sizeof(key);
    esp_now_add_peer(mac_addr,role,channel,key,key_len);
    
}

void loop() {

  //***GENERACION Y ENVIO DE NUMEROS (MAESTRO)***//
    if (engine){
      
      //***GENERACION NUMERO ALEATORIO A ENVIAR***//
        uint8_t num = random(0, 100);                  
        Serial.print("Number generated: "); Serial.println(num);
        
      //***ENVÍO DEL NUMERO ALEATORIO POR ESP-NOW (MAESTRO)***//
        uint8_t da[6] = {0x30, 0xAE, 0xA4, 0x37, 0x97, 0x91};
        uint8_t data[sizeof(num)]; memcpy(data, &num, sizeof(num));
        int len = sizeof(data);
        esp_now_send(da, data, len);
        
        delay(2000); //Si se pierden datos en la recepción se debe subir este valor
    
      //***VERIFICACIÓN DEL ESTADO DE RECEPCIÓN DE LOS DATOS POR EL ESCLAVO***//
        esp_now_register_send_cb([](uint8_t* mac, uint8_t status) {
          char MACesclavo[6];
          sprintf(MACesclavo,"%02X:%02X:%02X:%02X:%02X:%02X",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
          Serial.print("- Last Packet Sent to esp32: "); Serial.print(MACesclavo);
          Serial.print(" | Delivery (0K = 0 - ERROR = 1): "); Serial.println(status);
      });
      
    }
  
  //***RECEPCIÓN DE DATOS DE LA COMUNICACIÓN ESP-NOW (ESCLAVO)***//
     esp_now_register_recv_cb([](uint8_t *mac, uint8_t *data, uint8_t len) {

      //***MOSTRAR MAC Y DATOS RECIBIDOS***//
        char MACmaestro[6];
        sprintf(MACmaestro, "%02X:%02X:%02X:%02X:%02X:%02X",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
        Serial.print("-- Last Packet Received from ESP32: "); Serial.print(MACmaestro);

        uint8_t dataReceived;
        memcpy(&dataReceived, data, sizeof(dataReceived));
        Serial.print(" | Data: "); Serial.println(dataReceived); 

      //***ACTUACION EN FUNCION DEL DATO RECIBIDO***//
        //Enciende el LED y comienza a generar y enviar numeros aleatorios
        if (dataReceived == 1) {
          engine = true;
          digitalWrite(LED_BUILTIN, LOW);
          Serial.println("*** Engine started");
        }
        //Apaga el LED y para de generar y enviar numeros aleatorios
        else if (dataReceived == 0) {
          engine = false;
          digitalWrite(LED_BUILTIN, HIGH);
          Serial.println("*** Engine stopped");
        }
      
    });
  
}
