//***CODIGO PARA EL ESP32***//
//***DESARROLLADO POR DANIEL COUSIÑO Y NOEL SEARA***//

//***LIBRERIAS***//
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <esp_now.h>
#include <WiFi.h>

//***VARIABLES***//
BLECharacteristic *pCharacteristic;
bool deviceConnected = false;
bool engine = false;

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

#define CHANNEL 5
#define NUMSLAVES 2
esp_now_peer_info_t slaves[NUMSLAVES] = {};


/////////////////PARTE ESP-NOW//////////////////////


//***METODO INICIALIZACIÓN DEL PROTOCOLO ESP-NOW***//
void InitESPNow() {
  if (esp_now_init() == ESP_OK) {
    Serial.println("*** ESPNow Init Success");
  }
  else {
    Serial.println("*** ESPNow Init Failed");
    ESP.restart();  //Reinicia el ESP32
  }
}

//***METODO EMPAREJAMIENTO ESCLAVO***//
void manageSlave() {
   
    for (int i = 0; i < NUMSLAVES; i++) {
      const esp_now_peer_info_t *peer = &slaves[i];
      const uint8_t *peer_addr = slaves[i].peer_addr;
      Serial.print("*** Processing: ");
      for (int ii = 0; ii < 6; ++ii ) {
        Serial.print((uint8_t) slaves[i].peer_addr[ii], HEX);
        if (ii != 5) Serial.print(":");
      }
      Serial.print(" | Status: ");
      //Comprobar si ya existe el esclavo
      bool exists = esp_now_is_peer_exist(peer_addr);
      //Esclavo ya emparejado
      if (exists) {
        Serial.println("Already Paired");
      } 
      // Esclavo no emparejado, intento de emparejamiento
      else {
        esp_err_t addStatus = esp_now_add_peer(peer);
        //Emparejamiento correcto
        if (addStatus == ESP_OK) {
          Serial.println("Pair success");
        } 
        //Emparejamiento incorrecto
        else if (addStatus == ESP_ERR_ESPNOW_NOT_INIT) {
          Serial.println("ESPNOW Not Init");
        } else if (addStatus == ESP_ERR_ESPNOW_ARG) {
          Serial.println("Add Peer - Invalid Argument");
        } else if (addStatus == ESP_ERR_ESPNOW_FULL) {
          Serial.println("Peer list full");
        } else if (addStatus == ESP_ERR_ESPNOW_NO_MEM) {
          Serial.println("Out of memory");
        } else if (addStatus == ESP_ERR_ESPNOW_EXIST) {
          Serial.println("Peer Exists");
        } else {
          Serial.println("Not sure what happened");
        }
        delay(100);
      }
    }
}

//***METODO RECEPCION DATOS ESP-NOW (ESCLAVO)***//
void OnDataRecv(const uint8_t *mac_addr, const uint8_t *data, int data_len) {
  
  char macStr[18];
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  //Si el ESP32 tiene un cliente Bluetooth
  if (deviceConnected) {
    String str = String(*data)+"#"+macStr;
    char dataSend[25];
    str.toCharArray(dataSend, 25);
    //Envia el valor recibido al cliente Bluetooth
    pCharacteristic->setValue(dataSend);
    pCharacteristic->notify();
  }
  
  Serial.print("-- Last Packet Received from nodeMCU: "); Serial.print(macStr);
  Serial.print(" | Data: "); Serial.println(*data);
  
}

//***METODO ENVIO DATOS ESP-NOW (MAESTRO)***// 
void sendData(uint8_t parameter) {
  
  uint8_t dataSend = parameter;
  for (int i = 0; i < NUMSLAVES; i++) {
    const uint8_t *peer_addr = slaves[i].peer_addr;
    if (i == 0) {
      Serial.print("Sending: ");
      Serial.println(dataSend);
    }
    esp_err_t result = esp_now_send(peer_addr, &dataSend, sizeof(dataSend));
    //Comprobacion del envio de datos
    Serial.print("Send Status: ");
    //Datos enviados correctamente
    if (result == ESP_OK) {
      Serial.println("Success");
    } 
    //Datos no enviados
    else if (result == ESP_ERR_ESPNOW_NOT_INIT) {
      Serial.println("ESPNOW not Init.");
    } else if (result == ESP_ERR_ESPNOW_ARG) {
      Serial.println("Invalid Argument");
    } else if (result == ESP_ERR_ESPNOW_INTERNAL) {
      Serial.println("Internal Error");
    } else if (result == ESP_ERR_ESPNOW_NO_MEM) {
      Serial.println("ESP_ERR_ESPNOW_NO_MEM");
    } else if (result == ESP_ERR_ESPNOW_NOT_FOUND) {
      Serial.println("Peer not found.");
    } else {
      Serial.println("Not sure what happened");
    }
    delay(100);
  }
    
}

//***METODO COMPROBACION ESTADO RECEPCION DATOS ESP-NOW (MAESTRO)***// 
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  char macStr[18];
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.print("- Last Packet Sent to nodeMCU: "); Serial.print(macStr);
  Serial.print(" | Status: "); Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}


/////////////////PARTE BLUETOOTH/////////////////////////


//***METODO ESCUCHADOR DE SI HAY CLIENTE CONECTADO***// 
class MyServerCallbacks: public BLEServerCallbacks {

  //***CUANDO SE CONECTA UN CLIENTE***// 
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      Serial.println("*** Bluetooth client connected");
    };

  //***CUANDO SE DESCONECTA UN CLIENTE***// 
    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      Serial.println("*** Bluetooth client disconnected");
      //Si no se ha mandado orden de parar al esclavo, la manda
      if (engine) {
          Serial.println("*** Stopping system!");
          engine = false;
          WiFi.mode(WIFI_STA);
          manageSlave();
          for (int i = 0; i < 5; i++) {
              sendData(0);
              delay(1000);
          }
          WiFi.mode(WIFI_AP);
      }
    }
};

//***METODO ESCUCHADOR DE SI RECIBO DATOS***// 
class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
        
        std::string rxValue = pCharacteristic->getValue();

        //Si llegaron datos por bluetooth
        if (rxValue.length() > 0) {
          Serial.println("---------------------------------------------------------------------------------");
          Serial.print("-- Last Packet Received from Bluetooth: ");
          for (int i = 0; i < rxValue.length(); i++) {
            Serial.print(rxValue[i]);
          }

          Serial.println("");
  
          //***ENVIO DE DATOS A LOS NODEMCU EN FUNCION DEL DATO RECIBIDO POR BLUETOOTH***// 
          //Si se recibión ON, mandar orden al esclavo de arrancar
          if (rxValue.find("ON") != -1) { 
              Serial.println("*** Starting system!");
              engine = true;
              WiFi.mode(WIFI_STA);
              manageSlave();
              for (int i = 0; i < 5; i++) {
                sendData(1);
                delay(1000);
              }
              WiFi.mode(WIFI_AP);
              Serial.println("---------------------------------------------------------------------------------");
          }
          //Si se recibión OFF, mandar orden al esclavo de parar
          else if (rxValue.find("OFF") != -1) {
              Serial.println("*** Stopping system!");
              engine = false;
              WiFi.mode(WIFI_STA);
              manageSlave();
              for (int i = 0; i < 5; i++) {
                sendData(0);
                delay(1000);
              }
              WiFi.mode(WIFI_AP);
              Serial.println("---------------------------------------------------------------------------------");
          }

        }
        
}};


void setup() {

  //***INICIALIZACIÓN DEL PUERTO SERIE***//
    Serial.begin(115200); delay(10);
    Serial.println("");

  
  /////////////////PARTE ESP-NOW//////////////////////


  //***WIFI EN MODO PUNTO DE ACCESO (ESCLAVO)***//
    WiFi.mode(WIFI_AP);
    
  //***INICIALIZACIÓN DEL ESP-NOW***//
    InitESPNow();
    
  //***DATOS DE LA MAC del ESP32***// 
    Serial.print("Access Point MAC: "); Serial.println(WiFi.softAPmacAddress());

  //***ASIGNACION METODOS RECEPCION Y ENVIO DATOS ESP-NOW***// 
    esp_now_register_recv_cb(OnDataRecv);
    esp_now_register_send_cb(OnDataSent);

  //***AGREGAR ESCLAVOS***// 
    uint8_t mac_addr[6] = {0x68, 0xC6, 0x3A, 0x89, 0x99, 0x06}; //Dirección MAC STA de los NODEMCU esclavos
    uint8_t mac_addr2[6] = {0xEE, 0xFA, 0xBC, 0x05, 0x69, 0x1F}; //Dirección MAC STA de los NODEMCU esclavos
    for (int i = 0; i < 6; ++i ) {
        slaves[0].peer_addr[i] = mac_addr[i];
        slaves[1].peer_addr[i] = mac_addr2[i];
    }
    slaves[0].channel = CHANNEL;
    slaves[0].encrypt = 0;
    slaves[1].channel = CHANNEL;
    slaves[1].encrypt = 0;
  
    
  /////////////////PARTE BLUETOOTH/////////////////////////
  

  //***ASIGNAR NOMBRE AL BLUETOOTH***// 
    BLEDevice::init("ESP32_DANIEL_NOEL");
  
  //***CREAR SERVIDOR BLUETOOTH***// 
    BLEServer *pServer = BLEDevice::createServer();
    pServer->setCallbacks(new MyServerCallbacks());
  
  //***CREAR SERVICIO BLUETOOTH***// 
    BLEService *pService = pServer->createService(SERVICE_UUID);
  
  //***CREAR CARACTERISTICAS BLUETOOTH***// 
    pCharacteristic = pService->createCharacteristic(
                        CHARACTERISTIC_UUID_TX,
                        BLECharacteristic::PROPERTY_NOTIFY
                      );
                        
    pCharacteristic->addDescriptor(new BLE2902());
  
    BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                           CHARACTERISTIC_UUID_RX,
                                           BLECharacteristic::PROPERTY_WRITE
                                         );
  
    pCharacteristic->setCallbacks(new MyCallbacks());
  
  //***INCIAR SERVICIO BLUETOOTH***// 
    pService->start();
  
  //***INCIAR AVISOS BLUETOOTH***// 
    pServer->getAdvertising()->start();
    
    Serial.println("*** Waiting a Bluetooth client connection to notify...");
    Serial.println("---------------------------------------------------------------------------------");

}

void loop() {
  // Chill
}
