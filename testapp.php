<?php

	//Obtener el valor q recibido
	$q = $_REQUEST["q"];

	//Si el valor recibido es #REVMOVE# elimina el fichero .log
	if ($q == "#REMOVE#") {
		if (!unlink("logs.txt"))
		{
		  echo ("#LOG NOT DELETED#");
		}
		else
		{
		  echo ("#LOG DELETED#");
		}
	} 
	//Si no, devuelve el valor entre sharps y registra en el .log la fecha y hora, IP publica y el valor
	else {
		echo "#".$q."#";
		//Obtencion de la IP publica
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		//Guardar registro en el .log
		$myfile = fopen("logs.txt", "a") or die("Unable to open file!");
		$txt =  date('Y/m/d H:i:s'). " <> ".$ip. " <> ". $q . "\r\n";
		fwrite($myfile, $txt);
		fclose($myfile);
	}

?> 